<?php
declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use PHPUnit\Framework\TestCase;

class ArticleRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldListArticles()
    {
        $articleRepository = new ArticleRepository();
        $articleList = $articleRepository->findAll();
        $this->assertCount(1,$articleList);
        $this->assertContainsOnlyInstancesOf(Article::class,$articleList);
    }
}
