<?php
namespace App\Controller;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $articleRepository;
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $output = '';

        foreach ($this->articleRepository->findAll() as $article) {
            $output .= '<p class="article">'.$article->getText().'</p>';
        }

        if(empty($output)) {
            $output = '<p class="no-article">Aucun article.</p>';
        }

        return new Response('<html>
            <body>
            <h1>Accueil du blog</h1>
            ' . $output . '
            </body>
            </html>');

    }
}
